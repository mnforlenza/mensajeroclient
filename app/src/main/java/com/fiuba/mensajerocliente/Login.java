package com.fiuba.mensajerocliente;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.os.AsyncTask;
import android.widget.Button;
import android.widget.EditText;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.fiuba.mensajerocliente.client.RestClient;
import com.fiuba.mensajerocliente.dto.Message;
import com.fiuba.mensajerocliente.dto.ResponseError;
import com.fiuba.mensajerocliente.dto.User;
import com.fiuba.mensajerocliente.dto.Response;
import com.fiuba.mensajerocliente.services.ApiService;
import com.fiuba.mensajerocliente.services.Application;
import com.fiuba.mensajerocliente.services.RestServiceAsync;
import com.fiuba.mensajerocliente.session.SessionManager;
import com.gc.materialdesign.widgets.Dialog;
import com.gc.materialdesign.widgets.ProgressDialog;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.RegexpValidator;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Login extends ActionBarActivity {

    private Toolbar toolbar;
    private SessionManager session;
    private Dialog dialog;
    private ProgressDialog progressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        session = new SessionManager(getApplicationContext());

        if (session.isLoggedIn())
        {
            Intent intent = new Intent(Login.this, Main.class);
            startActivity(intent);
        }


        Button button = (Button) findViewById(R.id.login);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String user  =  ((EditText)findViewById(R.id.textuser)).getText().toString();
                String password =  ((EditText)findViewById(R.id.textpassword)).getText().toString();

                if (user.isEmpty() || password.isEmpty()) {
                    ((MaterialEditText) findViewById(R.id.textuser)).validateWith(new RegexpValidator("El nombre de usuario no puede ser vacio.", "\\d+"));
                    ((MaterialEditText) findViewById(R.id.textpassword)).validateWith(new RegexpValidator("La contraseña no puede ser vacia.", "\\d+"));
                }
                else
                {
                    authenticate();
                }

            }
        });

        Button singup = (Button) findViewById(R.id.signUp);
        singup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent intent = new Intent(Login.this, Registration.class);
                    startActivity(intent);
            }
        });


        progressDialog =  new ProgressDialog(this, "Iniciando sesión");

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }


    @Subscribe
    public void onResponse(Response response) {
        Application.getEventBus().unregister(this);

        progressDialog.dismiss();
        if(response != null && response.succes) {
            String user  =  ((EditText)findViewById(R.id.textuser)).getText().toString();
            session.createLoginSession("",user,"", response.result.token,"","");
            Intent intent = new Intent(Login.this, Main.class);
            startActivity(intent);

        }else {
            dialog = new Dialog(Login.this, null, "Credenciales incorrectas");
            dialog.show();
            dialog.getButtonAccept().setText("Aceptar");
        }

    }

    //Se llama a este metodo en caso de que la api devuelva cualquier tipo de error
    @Subscribe
    public void onResponse(ResponseError responseError) {
        Application.getEventBus().unregister(this);
        progressDialog.dismiss();
        Toast.makeText(this, "Hubo un error al intentar loguerse." + responseError.reason, Toast.LENGTH_SHORT).show();
    }

    public void authenticate() {
        progressDialog.show();
        Application.getEventBus().register(this);


        final String user  =  ((EditText)findViewById(R.id.textuser)).getText().toString();
        final String password =  ((EditText)findViewById(R.id.textpassword)).getText().toString();

        RestServiceAsync.GetResult result = new RestServiceAsync.GetResult<Response, ApiService>() {
            @Override
            public Response getResult(ApiService service) {
                return service.login(new User(user, password));
            }
        };


        RestClient restClient = new RestClient();

        RestServiceAsync callApi = new RestServiceAsync<List<Message>, ApiService>();
        callApi.fetch(restClient.getApiService(), result, new ResponseError());

    }

}
