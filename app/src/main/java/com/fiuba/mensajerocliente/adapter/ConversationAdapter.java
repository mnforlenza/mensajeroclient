package com.fiuba.mensajerocliente.adapter;

import android.content.Context;
import android.media.MediaExtractor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fiuba.mensajerocliente.R;
import com.fiuba.mensajerocliente.dto.Conversation;
import com.fiuba.mensajerocliente.dto.Message;
import com.fiuba.mensajerocliente.extras.Constants;

import java.util.ArrayList;
import java.util.List;

public class ConversationAdapter extends RecyclerView.Adapter<ConversationAdapter.ViewHolderConversations> {
    private LayoutInflater layoutInflater;
    private List<Message> conversationItems;
    private Context context;
    private String userId;

    public ConversationAdapter(Context context){
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }


    public void setConversations(List<Message> conversations, String userId) {
        this.conversationItems = conversations;
        //this.setConversationMocks();
        this.userId = userId;
        sortMark();
        notifyDataSetChanged();
    }

    private void setConversationMocks(){
        this.conversationItems = new ArrayList<Message>();
        conversationItems.add(new Message("hola Gonza Como estas?", "gonzalo","jimena", Constants.MsgCardType.text));
    }

    public void sortMark() {
        if(conversationItems.isEmpty()) {
            return;
        }

        //TODO: orden alfabeticamente por nombre de usuario
        //Collections.sort(groupItems);
    }

    @Override
    public ConversationAdapter.ViewHolderConversations onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_conversation, parent, false);
        ViewHolderConversations viewHolder = new ViewHolderConversations(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ConversationAdapter.ViewHolderConversations holder, int position) {

        Message conversationItem = conversationItems.get(position);

        holder.textViewTitle.setText(conversationItem.userTo);
        holder.textLastMsg.setText(conversationItem.content);
    }

    @Override
    public int getItemCount() {
        return conversationItems.size();
    }

    public Message getConversation(int position)
    {
        return conversationItems.get(position);
    }


    static class ViewHolderConversations extends RecyclerView.ViewHolder {

        TextView textViewTitle;
        //ImageView imageViewConversation;
        TextView textLastMsg;

        public ViewHolderConversations(View itemView) {
            super(itemView);
            textViewTitle = (TextView)itemView.findViewById(R.id.conversation_title);
            //imageViewConversation = (ImageView) itemView.findViewById(R.id.image_conversation);
            textLastMsg = (TextView)itemView.findViewById(R.id.lastMsgText);

        }
    }
}
