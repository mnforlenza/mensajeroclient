package com.fiuba.mensajerocliente.adapter;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.dexafree.materialList.cards.BasicImageButtonsCard;
import com.dexafree.materialList.cards.BigImageButtonsCard;
import com.dexafree.materialList.cards.OnButtonPressListener;
import com.dexafree.materialList.cards.SimpleCard;
import com.dexafree.materialList.controller.IMaterialListAdapter;
import com.dexafree.materialList.controller.OnDismissCallback;
import com.dexafree.materialList.model.Card;
import com.dexafree.materialList.view.MaterialListView;
import com.fiuba.mensajerocliente.R;
import com.fiuba.mensajerocliente.client.RestClient;
import com.fiuba.mensajerocliente.customcard.TextCard;
import com.fiuba.mensajerocliente.dto.Message;
import com.fiuba.mensajerocliente.extras.Utils;
import com.gc.materialdesign.widgets.ProgressDialog;

import java.util.List;

import retrofit.client.Response;

public class MessageAdapter {
    private LayoutInflater layoutInflater;
    private List<Message> msgItems;
    private Context context;
    private String userId;
    private MaterialListView materialListView;


    public MessageAdapter(Context context, MaterialListView materialListView , String userId){
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.materialListView = materialListView;
        this.userId = userId;

        setDismissCallback();
    }

    public void fillArray() {
        materialListView.clear();
        for (int i = 0; i < this.msgItems.size(); i++) {
            Card card = getRandomCard(this.msgItems.get(i));
            materialListView.add(card);
        }
    }

    private Card getRandomCard(Message msg) {
        //String title = msg.user.name + " " + msg.user.username;
        String title = msg.userFrom;
        final String idMessage = msg._id;

        int position = 0;
        SimpleCard card;

        switch (msg.typeOf) {

            case place:
                card = new BasicImageButtonsCard(this.context);
                card.setDescription(msg.date);
                card.setTitle(title);
                Drawable drawablePlace = new BitmapDrawable(context.getResources(), Utils.getPhoto(msg.content));
                card.setDrawable(drawablePlace);
                card.setTag("PLACE_CARD");
                card.setDismissible(true);
                ((BasicImageButtonsCard) card).setLeftButtonText("Borrar");
                ((BasicImageButtonsCard) card).setLeftButtonTextColorRes(R.color.my_awesome_darker_color);
                ((BasicImageButtonsCard) card).setRightButtonText("");
                ((BasicImageButtonsCard) card).setOnLeftButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        deleteCard(idMessage, card);
                    }
                });

                return card;

            case text:
                card = new TextCard(this.context, userId, userId, msg.date);
                card.setDescription(msg.content);
                card.setTitle(title);
                card.setTag("TEXT_CARD");

                ((TextCard)card).setOnButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        deleteCard(idMessage, card);
                    }
                });

                card.setDismissible(((TextCard) card).isDeleteable());
                return card;

            default:
                card = new BigImageButtonsCard(context);
                return card;

        }}

    private void setDismissCallback() {
        OnDismissCallback callback = new OnDismissCallback() {
            @Override
            public void onDismiss(Card card, int i) {
                switch(card.getTag().toString()) {
                    case "TEXT_CARD":
                        OnButtonPressListener button = ((TextCard)card).getOnButtonPressedListener();
                        button.onButtonPressedListener(null, card);
                        break;
                    case "PLACE_CARD":
                        //OnButtonPressListener button2 = ((PlaceCard)card).getOnButtonPressedListener();
                        //button2.onButtonPressedListener(null, card);
                        break;
                }
            }
        };
        materialListView.setOnDismissCallback(callback);
    }

    private void deleteCard(String idMessage, Card card) {
        //TODO: delete task
        //DeleteMsgTask deleteMsg = new DeleteMsgTask(idMessage, card);
        //deleteMsg.execute();
    }

    public void setData(List<Message> msgs) {
        this.msgItems = msgs;
    }

    public void delete(Card card) {
        ((IMaterialListAdapter)materialListView.getAdapter()).remove(card,false);
    }

    public void addMsg(Message card){
        materialListView.add(getRandomCard(card));
    }

    private class DeleteMsgTask extends AsyncTask<Void, Void,Response> {
        RestClient restClient;
        private String idMessage;
        private ProgressDialog progressDialog;
        private Card card;

        public DeleteMsgTask(String idMessage, Card card)
        {
            progressDialog =  new ProgressDialog(context.getApplicationContext(), "Borrando mensaje.");
            this.idMessage = idMessage;
            this.card = card;
        }

        public void executeTask() {
            try {
                this.execute();
            } catch (Exception e) {
                Toast.makeText(context.getApplicationContext(), "Error.", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onPreExecute() {
            restClient = new RestClient();
            progressDialog.show();
        }

        @Override
        protected retrofit.client.Response doInBackground(Void... params) {
            retrofit.client.Response response = null;
            try {
                //TODO: posteo de mensajes

            } catch (Exception ex) {

            }
            return response;
        }


        @Override
        public void onPostExecute(retrofit.client.Response response) {
            progressDialog.dismiss();
            if (response== null)
                Toast.makeText(context.getApplicationContext(), "Hubo un error al borrar el mensaje.", Toast.LENGTH_SHORT).show();
            else
                delete(card);
        }
    }
}
