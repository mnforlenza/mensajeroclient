package com.fiuba.mensajerocliente.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fiuba.mensajerocliente.R;
import com.fiuba.mensajerocliente.dto.User;

import java.util.ArrayList;
import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolderContacts> {
    private LayoutInflater layoutInflater;
    private List<User> contactsItems;
    private Context context;
    private String userId;

    public ContactAdapter(Context context){
        layoutInflater = LayoutInflater.from(context);
        contactsItems = new ArrayList<User>();
        this.context = context;
    }


    public void setContacts(List<User> listContacts,String userId) {

        for(int i = 0; i < listContacts.size(); i++) {
            if (!listContacts.get(i).userName.equals(userId))
                this.contactsItems.add(listContacts.get(i));
        }
        this.userId = userId;
        notifyDataSetChanged();
    }

    @Override
    public ContactAdapter.ViewHolderContacts onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_contact, parent, false);
        ViewHolderContacts viewHolder = new ViewHolderContacts(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ContactAdapter.ViewHolderContacts holder, int position) {

        User contactItem = contactsItems.get(position);

        holder.textViewName.setText(contactItem.userName);
        holder.textMail.setText(contactItem.email);

        holder.viewSeparator.setBackgroundColor(Color.WHITE);


        if(contactsItems.get(0) != contactItem)
        {
            // los item que muestran la letra y son distintos al primero muestran la division
            holder.viewSeparator.setBackgroundColor(Color.parseColor("#ffcfcfcf"));
        }

        if(contactItem.online)
        {
            holder.textOnline.setVisibility(View.VISIBLE);
        }else
        {
            holder.textOnline.setVisibility(View.GONE);
        }

        Bitmap  icon;
        if (contactItem.photo!= null && !contactItem.photo.isEmpty())
        {
            try {

                byte[] decodedString = Base64.decode(contactItem.photo, Base64.DEFAULT);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPurgeable = true;
                icon = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length, options);
                icon = Bitmap.createScaledBitmap(icon, 50, 50, true);
                holder.imageViewContact.setImageBitmap(icon);

            }catch (Exception ex)
            {
            }

        }else {
            icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_account_circle_grey_48dp);
            holder.imageViewContact.setImageBitmap(icon);

        }

    }

    @Override
    public int getItemCount() {
        return contactsItems.size();
    }

    public User getContact(int position) {
        return contactsItems.get(position);
    }


    static class ViewHolderContacts extends RecyclerView.ViewHolder {

        TextView textViewName;
        ImageView imageViewContact;
        View viewSeparator;
        ImageView viewSendInvitation;
        TextView textOnline;
        TextView textMail;


        public ViewHolderContacts(View itemView) {
            super(itemView);
            textViewName = (TextView)itemView.findViewById(R.id.text_name_contact);
            imageViewContact = (ImageView) itemView.findViewById(R.id.image_contact);
            viewSeparator = (View) itemView.findViewById(R.id.separatorContact);
            viewSendInvitation = (ImageView) itemView.findViewById(R.id.sendInvitation);
            textOnline = (TextView)itemView.findViewById(R.id.textOnline);
            textMail = (TextView)itemView.findViewById(R.id.mailText);

        }
    }
}
