package com.fiuba.mensajerocliente;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fiuba.mensajerocliente.adapter.ContactAdapter;
import com.fiuba.mensajerocliente.client.RestClient;
import com.fiuba.mensajerocliente.dto.Message;
import com.fiuba.mensajerocliente.dto.ResponseError;
import com.fiuba.mensajerocliente.dto.User;
import com.fiuba.mensajerocliente.dto.UserList;
import com.fiuba.mensajerocliente.extras.Constants;
import com.fiuba.mensajerocliente.extras.RecyclerItemClickListener;
import com.fiuba.mensajerocliente.services.ApiService;
import com.fiuba.mensajerocliente.services.Application;
import com.fiuba.mensajerocliente.services.RestServiceAsync;
import com.fiuba.mensajerocliente.session.SessionManager;
import com.gc.materialdesign.widgets.Dialog;
import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;


public class ContactMessage extends ActionBarActivity {

    private Toolbar toolbar;
    private String content;
    private Dialog dialog;
    private ContactAdapter contactAdapter;
    private RecyclerView recyclerView;
    private EditText searchText;
    private ProgressBar prgrsBar;
    private TextView emptyView;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_layout);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        session = new SessionManager(this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        prgrsBar = (ProgressBar) findViewById(R.id.progressBarCircularIndeterminate);
        emptyView = (TextView) findViewById(R.id.empty_view);


        recyclerView = (RecyclerView) findViewById(R.id.listViewContacts);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));


        contactAdapter = new ContactAdapter(getApplicationContext());
        //addAllContacts(fillContacts());
        recyclerView.setAdapter(contactAdapter);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        User user = contactAdapter.getContact(position);
                        Message conversation = new Message("",session.getUserName(),user.userName, Constants.MsgCardType.text);
                        Intent intent = new Intent(getApplicationContext(), ContactConversation.class);
                        intent.putExtra("CONVERSATION", new Gson().toJson(conversation));
                        startActivity(intent);
                    }
                })
        );



        contactAdapter.setContacts(new ArrayList<User>(), session.getUserName());

        getUsers();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem itemSubmit = menu.findItem(R.id.action_submit);
        itemSubmit.setVisible(false);
        MenuItem itemSearch = menu.findItem(R.id.action_edit);
        itemSearch.setVisible(false);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        Application.getEventBus().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.getEventBus().register(this);
    }

    @Subscribe
    public void onUserResponse(UserList userList) {
        if(userList.users.isEmpty())
            emptyView.setVisibility(View.VISIBLE);
        else
            emptyView.setVisibility(View.INVISIBLE);

        prgrsBar.setVisibility(View.INVISIBLE);
        contactAdapter.setContacts(userList.users,session.getUserName());
    }

    //Se llama a este metodo en caso de que la api devuelva cualquier tipo de error
    @Subscribe
    public void onResponse(ResponseError responseError) {
        emptyView.setVisibility(View.INVISIBLE);
        prgrsBar.setVisibility(View.INVISIBLE);
        Toast.makeText(getApplicationContext(), "Hubo un error al obtener los datos del usuario." + responseError.reason, Toast.LENGTH_SHORT).show();
    }



    public void getUsers() {

        prgrsBar.setEnabled(true);
        prgrsBar.setVisibility(View.VISIBLE);

        RestServiceAsync.GetResult result = new RestServiceAsync.GetResult<UserList, ApiService>() {
            @Override
            public UserList getResult(ApiService service) {
                return service.getUsers();
            }
        };

        RestClient restClient = new RestClient();

        RestServiceAsync callApi = new RestServiceAsync<UserList, ApiService>();
        callApi.fetch(restClient.getApiService(), result, new ResponseError());

    }

}