package com.fiuba.mensajerocliente.extras;

public class Constants {
    public static final String NAME = "name";
    public static final String LASTNAME = "lastname";
    public static final String EMAIL = "email";
    public static final String PHONE = " phone";
    public static final String NATIONALITY = "nationality";
    public static final String BIRTHDAY = " birthday";
    public static final String GENDER = "gender";
    public static final String CHECKIN = "checkin";
    public static final String PHOTO = "photo";
    public static final String COMENTARIO = "comentario";
    public static final String[] EXTENSIONES = {"png", "jpeg"};
    public static final String FORMAT_DATETIME = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String USER = "user";
    public static final String TOKEN = "token";
    public static final String PAGE = "page";
    public static final String EDIT = "edit";
    public static final int MAX_SIZE_PHOTO = 8;
    public static enum MsgCardType {text, photo, video, place, file, link};


}
