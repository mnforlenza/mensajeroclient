package com.fiuba.mensajerocliente.extras;

import android.content.Context;
import android.util.AttributeSet;

import com.fiuba.mensajerocliente.R;
import com.gc.materialdesign.views.ButtonFloat;


public class ButtonFloatMaterial extends ButtonFloat {

    public ButtonFloatMaterial(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBackgroundResource(R.drawable.back_button_float);

    }
}
