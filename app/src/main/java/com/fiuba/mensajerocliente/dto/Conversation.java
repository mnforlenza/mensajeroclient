package com.fiuba.mensajerocliente.dto;

public class Conversation {
    public String _id;
    public String content;
    public String date;
    public User user;
    public String userName;

    public Conversation(String content, String userName){
        this.content = content;
        this.userName = userName;
    }
}
