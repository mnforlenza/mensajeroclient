package com.fiuba.mensajerocliente.dto;

import com.fiuba.mensajerocliente.extras.Constants;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Message {
    public String _id;
    public String content;
    public String date;
    public String userFrom;
    public String userTo;
    public Constants.MsgCardType typeOf;



    public Message(String content, String userFrom, String userTo ,Constants.MsgCardType typeOf){
        this.content = content;
        this.userFrom = userFrom;
        this.userTo = userTo;
        this.typeOf = typeOf;
        this.setTodayDate();

    }

    private void setTodayDate(){
        SimpleDateFormat format1 = new SimpleDateFormat("d MMM yyyy HH:mm:ss");
        this.date = format1.format(Calendar.getInstance().getTime());
    }
}
