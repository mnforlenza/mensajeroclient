package com.fiuba.mensajerocliente.dto;

public class Checkin {

    public String userName;
    public String latitude;
    public String longitude;

    public Checkin(String userName, String latitude, String longitude) {
        this.userName = userName;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
