package com.fiuba.mensajerocliente.dto;

public class User {

    public String userName;
    public String password;
    public String email;
    public String name;
    public String photo;
    public String comments;
    public String nacionality;
    public String city;
    public String gender;
    public String birthday;
    public String mobile;
    public String location;
    public String locationPlace;
    public String locationPlaceDescription;
    public String _id;
    public boolean active;
    public boolean online;

    public User(String name, String username, String password, String email, String photo, String comments, String nationality, String city, String birthday, String gender, String phones){
        this.name = name;
        this.userName = username;
        this.password = password;
        this.email = email;
        this.photo = photo;
        this.comments = comments;
        this.nacionality = nationality;
        this.city = city;
        this.birthday = birthday;
        this.gender = gender;
        this.mobile = phones;
    }

    public User(String username, String name, String password, String email){
        this.name = name;
        this.userName = username;
        this.password = password;
        this.email = email;
    }

    public User(String username, String password){
        this.userName = username;
        this.password = password;
    }

    public User(String username, String email, String photo){
        this.email = email;
        this.userName = username;
        this.photo = photo;
    }

}
