package com.fiuba.mensajerocliente;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import static com.fiuba.mensajerocliente.extras.Constants.*;

import com.fiuba.mensajerocliente.client.RestClient;
import com.fiuba.mensajerocliente.dto.Response;
import com.fiuba.mensajerocliente.dto.User;
import com.fiuba.mensajerocliente.extras.Utils;
import com.fiuba.mensajerocliente.fragment.IProfile;
import com.fiuba.mensajerocliente.session.SessionManager;
import com.gc.materialdesign.widgets.Dialog;
import com.google.gson.Gson;

public class ProfileReduced extends ActionBarActivity implements IProfile {
    private Toolbar toolbar;
    private ImageView photo;
    private TextView name;
    private TextView email;
    private TextView lastcheckin;
    private TextView fieldTxt2;
    private TextView fieldTxt3;
    private TextView fieldTxt4;
    private String _id;
    private ImageView icon2;
    private ImageView icon3;
    private ImageView icon4;
    private int position;
    private SessionManager session;
    private Dialog dialog;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_reduced);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        session = new SessionManager(getApplicationContext());

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
        initialize();

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            String userJson = extras.getString(USER);
            User user = new Gson().fromJson(userJson, User.class);
            setUser(user);
        }

    }

    private void initialize() {
        position = 2;
        photo = (ImageView) findViewById(R.id.imageViewR);
        name = (TextView) findViewById(R.id.nombre_ProfileR);
        email = (TextView) findViewById(R.id.emailProfileR);
        lastcheckin = (TextView) findViewById(R.id.ultimoCheckin);

        fieldTxt2 = (TextView) findViewById(R.id.emailProfileR);
        fieldTxt3 = (TextView) findViewById(R.id.celularProfileR);
        fieldTxt4 = (TextView) findViewById(R.id.ultimoCheckin);

        icon2 = (ImageView)findViewById(R.id.ic_email);
        icon3 = (ImageView)findViewById(R.id.ic_mobile);
        icon4 = (ImageView)findViewById(R.id.ic_checkin);

        icon2.setVisibility(View.INVISIBLE);
        icon3.setVisibility(View.INVISIBLE);
        icon4.setVisibility(View.INVISIBLE);
    }

    private void loadData(User user) {
        getSupportActionBar().setTitle(user.userName);
        name.setText(user.userName);

        String email = user.email;
        if(email != null && !email.isEmpty()) {
            loadField(getResource(EMAIL), email);
        }

        String lastcheckinText = user.locationPlaceDescription;
        if(lastcheckinText != null && !lastcheckinText.isEmpty()) {
            loadField(getResource(CHECKIN), lastcheckinText);
        }

        String phoneText = user.mobile;
        if(phoneText != null && !phoneText.isEmpty()) {
            loadField(getResource(PHONE), phoneText);
        }

        String path = user.photo;
        if(path != null && !path.isEmpty()) {
            photo.setImageBitmap(Utils.getPhoto(user.photo));
        }

        _id = user._id;
    }

    private void loadField(int src, String txt) {
        position++;
        switch (position - 1) {
            case(2):
                icon2.setImageResource(src);
                fieldTxt2.setText(txt);
                icon2.setVisibility(View.VISIBLE);
                break;
            case(3):
                icon3.setImageResource(src);
                fieldTxt3.setText(txt);
                icon3.setVisibility(View.VISIBLE);
                break;
            case(4):
                icon4.setImageResource(src);
                fieldTxt4.setText(txt);
                icon4.setVisibility(View.VISIBLE);
                break;
        }
    }

    private int getResource(String value) {
        switch (value) {
            case(PHONE): return R.drawable.ic_phone_iphone_grey600_24dp;
            case(CHECKIN): return R.drawable.ic_location_history_grey600_18dp;
            case(EMAIL): return R.drawable.ic_email_grey600_24dp;
            default: return R.drawable.ic_person_grey;
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem itemSubmit = menu.findItem(R.id.action_submit);
        itemSubmit.setVisible(false);
        MenuItem itemSearch = menu.findItem(R.id.action_edit);
        itemSearch.setVisible(false);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setUser(User user) {
        if(user == null) {
            Toast.makeText(this, "Hubo un error al obtener los datos del usuario.", Toast.LENGTH_SHORT).show();
            return;
        }
        loadData(user);
    }

}


