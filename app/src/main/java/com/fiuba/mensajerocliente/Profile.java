package com.fiuba.mensajerocliente;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.fiuba.mensajerocliente.client.RestClient;
import com.fiuba.mensajerocliente.dto.Message;
import com.fiuba.mensajerocliente.dto.ResponseError;
import com.fiuba.mensajerocliente.dto.User;
import com.fiuba.mensajerocliente.dto.UserResponse;
import com.fiuba.mensajerocliente.extras.Utils;
import com.fiuba.mensajerocliente.fragment.IProfile;
import com.fiuba.mensajerocliente.services.ApiService;
import com.fiuba.mensajerocliente.services.Application;
import com.fiuba.mensajerocliente.services.RestServiceAsync;
import com.fiuba.mensajerocliente.session.SessionManager;
import com.fiuba.mensajerocliente.extras.ButtonFloatMaterial;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import static com.fiuba.mensajerocliente.extras.Utils.checkSizePhoto;
import static com.fiuba.mensajerocliente.extras.Utils.getPhotoString;
import static com.fiuba.mensajerocliente.extras.Utils.getResizedBitmap;


public class Profile extends ActionBarActivity implements IProfile {

    private Toolbar toolbar;
    private User user;
    public static int RESULT_LOAD = 1;
    private SessionManager session;
    private EditText name;
    private EditText username;
    private EditText mail;
    private EditText phone;
    private ButtonFloatMaterial buttonImage;
    private SweetAlertDialog pDialog;
    private MenuItem itemSubmit;
    private MenuItem itemEdit;
    private Bitmap photoBitmap;
    private ImageView photoUser;




    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.loadpersonaldata_layout);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initializeIdComponents();

        session = new SessionManager(getApplicationContext());

        getUser();

    }

    private boolean validateFields() {
        //TODO: validaciones
        return true;
    }

    public void setUser(User user) {
        if(user == null) {
            Toast.makeText(this, "Hubo un error al obtener los datos del usuario.", Toast.LENGTH_SHORT).show();
            return;
        }

        this.user = user;
    }

    private void initializeIdComponents() {
        username = (EditText) findViewById(R.id.idname);
        phone = (EditText) findViewById(R.id.idphone);
        mail = (EditText) findViewById(R.id.idmail);
        username.setEnabled(false);
        phone.setEnabled(false);
        mail.setEnabled(false);
        buttonImage = (ButtonFloatMaterial) findViewById(R.id.buttonImage);
        buttonImage.setVisibility(View.GONE);
        photoUser = (ImageView) findViewById(R.id.idPhoto);
        buttonImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                loadImagefromGallery(v);
            }
        });
    }

    private void loadDataInComponents(User user){

        username.setText(user.userName);
        mail.setText(user.email);
        session.setUserPhoto(user.photo);
        setPhoto(user.photo);

    }

    private void loadImagefromGallery(View view) {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        this.startActivityForResult(galleryIntent, RESULT_LOAD);
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == RESULT_LOAD && resultCode == Activity.RESULT_OK && null != data) {
                Bitmap foto = getPhoto(data);
                if(checkSizePhoto(foto)) {
                    photoBitmap = getResizedBitmap(foto,200,200);
                    photoUser.setImageBitmap(photoBitmap);
                }
            }
        } catch (Exception e) {
        }

    }

    private Bitmap getPhoto(Intent data) {
        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        ContentResolver c = this.getContentResolver();
        Cursor cursor = c.query(selectedImage, filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String img_Decodable = cursor.getString(columnIndex);
        cursor.close();

        return  BitmapFactory.decodeFile(img_Decodable);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        itemSubmit = menu.findItem(R.id.action_submit);
        itemSubmit.setVisible(false);
        itemEdit = menu.findItem(R.id.action_edit);
        itemEdit.setVisible(true);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_edit:
                username.setEnabled(true);
                phone.setEnabled(true);
                mail.setEnabled(true);
                buttonImage.setVisibility(View.VISIBLE);
                item.setVisible(false);
                itemSubmit.setVisible(true);
                break;
            case R.id.action_submit:
                username.setEnabled(false);
                phone.setEnabled(false);
                mail.setEnabled(false);
                buttonImage.setVisibility(View.GONE);
                item.setVisible(false);
                itemEdit.setVisible(true);
                update();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void setPhoto(String photo) {
        if (!photo.isEmpty()) {
            photoBitmap = Utils.getPhoto(photo);
            photoUser.setImageBitmap(photoBitmap);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Subscribe
    public void onUserResponse(UserResponse response) {
        if(response != null && response.succes )
            loadDataInComponents(response.result.user);
        else
            Toast.makeText(this, "Hubo un error al cargar al usuario.", Toast.LENGTH_SHORT).show();
        Application.getEventBus().unregister(this);


    }

    //Se llama a este metodo en caso de que la api devuelva cualquier tipo de error
    @Subscribe
    public void onErrorResponse(ResponseError responseError) {
        Application.getEventBus().unregister(this);
        Toast.makeText(this, "Hubo un error al obtener al usuario." + responseError.reason, Toast.LENGTH_SHORT).show();

    }


    public void getUser() {
        Application.getEventBus().register(this);

        RestServiceAsync.GetResult result = new RestServiceAsync.GetResult<UserResponse, ApiService>() {
            @Override
            public UserResponse getResult(ApiService service) {
                return service.get(session.getUserName());
            }
        };

        RestClient restClient = new RestClient();

        RestServiceAsync callApi = new RestServiceAsync<UserResponse, ApiService>();
        callApi.fetch(restClient.getApiService(), result, new ResponseError());

    }


    public void update() {
        Application.getEventBus().register(this);

        RestServiceAsync.GetResult result = new RestServiceAsync.GetResult<UserResponse, ApiService>() {
            @Override
            public UserResponse getResult(ApiService service) {
                return service.updateUser(new User(session.getUserName(),mail.getText().toString(),getPhotoString(photoBitmap)));
            }
        };

        RestClient restClient = new RestClient();

        RestServiceAsync callApi = new RestServiceAsync<UserResponse, ApiService>();
        callApi.fetch(restClient.getApiService(), result, new ResponseError());

    }

}
