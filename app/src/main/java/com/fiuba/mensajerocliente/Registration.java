package com.fiuba.mensajerocliente;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fiuba.mensajerocliente.client.RestClient;
import com.fiuba.mensajerocliente.dto.Response;
import com.fiuba.mensajerocliente.dto.ResponseError;
import com.fiuba.mensajerocliente.dto.User;
import com.fiuba.mensajerocliente.services.ApiService;
import com.fiuba.mensajerocliente.services.Application;
import com.fiuba.mensajerocliente.services.RestServiceAsync;
import com.gc.materialdesign.widgets.Dialog;
import com.gc.materialdesign.widgets.ProgressDialog;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.RegexpValidator;

import cn.pedant.SweetAlert.SweetAlertDialog;

import com.fiuba.mensajerocliente.extras.ButtonFloatMaterial;
import com.squareup.otto.Subscribe;


/**
 * Created by gonzalovelasco on 29/3/15.
 */
public class Registration extends ActionBarActivity {

    private Toolbar toolbar;
    private TextView name;
    private TextView password;
    private TextView confirmPassword;
    private TextView email;
    private Dialog dialog;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_layout);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        name = (MaterialEditText) findViewById(R.id.editName);
        password = (MaterialEditText) findViewById(R.id.editPassword);
        confirmPassword = (MaterialEditText) findViewById(R.id.confirmPassword);
        email = (MaterialEditText) findViewById(R.id.editEmail);

        ButtonFloatMaterial singup = (ButtonFloatMaterial) findViewById(R.id.registrate);
        singup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkFields()) {
                    register();
                }
            }
        });
        progressDialog = new ProgressDialog(this, "Registrando");



    }

    private boolean checkFields() {

        if(isEmpty(password)) ((MaterialEditText) findViewById(R.id.editPassword)).validateWith(new RegexpValidator("Ingresá una contraseña.", "\\d+"));
        if(isEmpty(confirmPassword)) ((MaterialEditText) findViewById(R.id.confirmPassword)).validateWith(new RegexpValidator("Confirmala.", "\\d+"));
        if(isEmpty(name)) ((MaterialEditText) findViewById(R.id.editName)).validateWith(new RegexpValidator("Ingresá tu nombre.", "\\d+"));
        if(isEmpty(email)) ((MaterialEditText) findViewById(R.id.editEmail)).validateWith(new RegexpValidator("Ingresá tu email de fiuba.", "\\d+"));

        if(!isEmpty(name) && !isEmpty(password) && !isEmpty(confirmPassword) && !isEmpty(email)) {

            //coincidencia de contraseñas
            String pass1 = password.getText().toString();
            String pass2 = confirmPassword.getText().toString();
            if (!pass1.equals(pass2)) {
                ((MaterialEditText) findViewById(R.id.editPassword)).validateWith(new RegexpValidator("", "\\d+"));
                ((MaterialEditText) findViewById(R.id.confirmPassword)).validateWith(new RegexpValidator("Las contraseñas no coinciden.", "\\d+"));

                return false;
            } else {
                if (pass1.length() < 8) {
                    ((MaterialEditText) findViewById(R.id.editPassword)).validateWith(new RegexpValidator("Fijate el mínimo.", "\\d+"));
                    ((MaterialEditText) findViewById(R.id.confirmPassword)).validateWith(new RegexpValidator("Fijate el mínimo.", "\\d+"));
                    return false;
                }
            }

            //mail ingresado valido
            String emailstring = email.getText().toString();
            if (!emailstring.contains("@")) {
                ((MaterialEditText) findViewById(R.id.editEmail)).validateWith(new RegexpValidator("No es un email válido.", "\\d+"));
                return false;
            }

            return true;
        }

        return false;
    }

    private boolean isEmpty(TextView textview) {
        return (textview.getText().length() == 0);
    }



    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Subscribe
    public void onResponse(Response response) {
        progressDialog.dismiss();
        Application.getEventBus().unregister(this);

        if (response.succes) {
            dialog = new Dialog(Registration.this, "Registro exitoso!", "Ya podes ingresar.");
            dialog.setOnAcceptButtonClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Registration.this, Login.class);
                    startActivity(intent);
                }
            });
            dialog.show();
            dialog.getButtonAccept().setText("Aceptar");
        }else
        {
            dialog = new Dialog(Registration.this, null,"Esa casilla de correo ya está registrada.");
            dialog.show();
            dialog.getButtonAccept().setText("Aceptar");
        }
    }

    //Se llama a este metodo en caso de que la api devuelva cualquier tipo de error
    @Subscribe
    public void onResponseError(ResponseError responseError) {
        progressDialog.dismiss();
        Application.getEventBus().unregister(this);

        Toast.makeText(this, "Hubo un error al intentar registrarse." + responseError.reason, Toast.LENGTH_SHORT).show();
    }

    public void register() {
        progressDialog.show();
        Application.getEventBus().register(this);

        final String userName  =  ((EditText)findViewById(R.id.editName)).getText().toString();
        final String password =  ((EditText)findViewById(R.id.editPassword)).getText().toString();
        final String email =  ((EditText)findViewById(R.id.editEmail)).getText().toString();

        RestServiceAsync.GetResult result = new RestServiceAsync.GetResult<Response, ApiService>() {
            @Override
            public Response getResult(ApiService service) {
                return service.create(new User(userName, userName, password, email));
            }
        };


        RestClient restClient = new RestClient();

        RestServiceAsync callApi = new RestServiceAsync<Response, ApiService>();
        callApi.fetch(restClient.getApiService(), result, new ResponseError());

    }
}
