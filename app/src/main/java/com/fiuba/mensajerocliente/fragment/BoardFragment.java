package com.fiuba.mensajerocliente.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fiuba.mensajerocliente.R;

import net.yanzm.mth.MaterialTabHost;


public class BoardFragment extends Fragment {

    public static final int TAB_CONTACTS = 0;
    public static final int TAB_MESSAGES = 1;
    public static final int TAB_CHECKIN = 2;
    public static final int TAB_COUNT = 3;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =inflater.inflate(R.layout.board_fragment, container, false);


        MaterialTabHost tabHost = (MaterialTabHost) view.findViewById(R.id.tabs);
        tabHost.setType(MaterialTabHost.Type.FullScreenWidth);
//        tabHost.setType(MaterialTabHost.Type.Centered);
//        tabHost.setType(MaterialTabHost.Type.LeftOffset);

        MyPagerAdapter pagerAdapter = new MyPagerAdapter(getFragmentManager());
        for (int i = 0; i < pagerAdapter.getCount(); i++) {
            tabHost.addTab(pagerAdapter.getPageTitle(i));
        }
        tabHost.setCurrentTab(TAB_MESSAGES);

        final ViewPager viewPager = (ViewPager) view.findViewById(R.id.pager);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(TAB_MESSAGES);
        viewPager.setOnPageChangeListener(tabHost);

        tabHost.setOnTabChangeListener(new MaterialTabHost.OnTabChangeListener() {
            @Override
            public void onTabSelected(int position) {
                viewPager.setCurrentItem(position);
            }
        });
        return view;
    }
    class MyPagerAdapter extends FragmentPagerAdapter {

        String[] tabs = getResources().getStringArray(R.array.tabs);
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
            tabs = getResources().getStringArray(R.array.tabs);


        }

        @Override
        public Fragment getItem(int position) {
           // MyFragment myFragment = MyFragment.getInstance(position);
           // return myFragment;

            Fragment fragment = null;
            switch (position) {
                case TAB_CONTACTS:
                    fragment = ContactFragment.newInstance("", "");
                    break;
                case TAB_MESSAGES:
                    fragment = MessageFragment.newInstance("", "");
                    break;
                case TAB_CHECKIN:
                    fragment = CheckinFragment.newInstance("", "");
                    break;
            }
            return fragment;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            //return getResources().getStringArray(R.array.tabs)[position];
            return tabs[position];
        }


        @Override
        public int getCount() {
            return TAB_COUNT;
        }
    }

}
