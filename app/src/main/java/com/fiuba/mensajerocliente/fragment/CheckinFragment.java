package com.fiuba.mensajerocliente.fragment;

import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fiuba.mensajerocliente.extras.ButtonFloatMaterial;
import com.fiuba.mensajerocliente.services.Application;
import com.fiuba.mensajerocliente.session.SessionManager;
import com.gc.materialdesign.widgets.Dialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapView;

import com.fiuba.mensajerocliente.R;
import com.google.android.gms.maps.MapsInitializer;
import com.fiuba.mensajerocliente.client.RestClient;
import com.fiuba.mensajerocliente.dto.Checkin;
import com.fiuba.mensajerocliente.dto.ResponseError;
import com.fiuba.mensajerocliente.dto.UserResponse;
import com.fiuba.mensajerocliente.services.ApiService;
import com.fiuba.mensajerocliente.services.RestServiceAsync;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.otto.Subscribe;


public class CheckinFragment extends Fragment implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    private View myView;
    private MapView mMapView;
    private TextView textView;
    private GoogleMap googleMap;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private double currentLatitude;
    private double currentLongitude;
    private SessionManager session;
    private Dialog dialog;


    public static CheckinFragment newInstance(String param1, String param2) {
        CheckinFragment fragment = new CheckinFragment();
        Bundle args = new Bundle();
        //put any extra arguments that you may want to supply to this fragment
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        myView = inflater.inflate(R.layout.checkin_fragment, container, false);

        session = new SessionManager(getActivity().getApplicationContext());


        mMapView = (MapView) myView.findViewById(R.id.mapView);

        mMapView.onCreate(savedInstanceState);

        mMapView.onResume();// needed to get the map to display immediately

        textView = (TextView) myView.findViewById(R.id.ultimaUbicaion);

        textView.setText(session.getLocationPlace());



        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        googleMap = mMapView.getMap();

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

        ButtonFloatMaterial postLocation = (ButtonFloatMaterial) myView.findViewById(R.id.postPlace);
        postLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkin();
            }
        });

        return myView;

    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }


    private void handleNewLocation(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();

        LatLng latLng = new LatLng(currentLatitude, currentLongitude);

        //mMap.addMarker(new MarkerOptions().position(new LatLng(currentLatitude, currentLongitude)).title("Current Location"));
        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .title("Estoy acá!");
        googleMap.addMarker(options);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14));
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
        else {
            handleNewLocation(location);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(getActivity(), CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
        }
    }

    @Subscribe
    public void onResponse(UserResponse response) {
        Application.getEventBus().unregister(this);
        if (response.succes)
        {
            dialog = new Dialog(getActivity(), null,"Se ha registrado tu ubicación.");
            dialog.show();
            dialog.getButtonAccept().setText("Aceptar");
            session.setLocationPLace(response.result.user.locationPlaceDescription);

            textView.setText(session.getLocationPlace());

        }else
        {
            dialog = new Dialog(getActivity(), null,"Se ha producido un error.");
            dialog.show();
            dialog.getButtonAccept().setText("Aceptar");

        }
    }

    //Se llama a este metodo en caso de que la api devuelva cualquier tipo de error
    @Subscribe
    public void onResponseError(ResponseError responseError) {
        Application.getEventBus().unregister(this);
        Toast.makeText(getActivity(), "Hubo un error al intentar realizar el checkin." + responseError.reason, Toast.LENGTH_SHORT).show();
    }

    public void checkin() {
        Application.getEventBus().register(this);

        RestServiceAsync.GetResult result = new RestServiceAsync.GetResult<UserResponse, ApiService>() {
            @Override
            public UserResponse getResult(ApiService service) {
                return service.userCheckin(new Checkin(session.getUserName(),String.valueOf(currentLatitude),String.valueOf(currentLongitude)));
            }
        };


        RestClient restClient = new RestClient();

        RestServiceAsync callApi = new RestServiceAsync<UserResponse, ApiService>();
        callApi.fetch(restClient.getApiService(), result, new ResponseError());

    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }
}
