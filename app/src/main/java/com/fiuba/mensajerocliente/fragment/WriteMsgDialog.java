package com.fiuba.mensajerocliente.fragment;


import android.app.AlertDialog;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fiuba.mensajerocliente.R;
import com.fiuba.mensajerocliente.adapter.ConversationAdapter;
import com.fiuba.mensajerocliente.adapter.MessageAdapter;
import com.fiuba.mensajerocliente.client.RestClient;
import com.fiuba.mensajerocliente.dto.BroadcastResponse;
import com.fiuba.mensajerocliente.dto.Message;
import com.fiuba.mensajerocliente.dto.Response;
import com.fiuba.mensajerocliente.dto.ResponseError;
import com.fiuba.mensajerocliente.extras.Constants;
import com.fiuba.mensajerocliente.services.ApiService;
import com.fiuba.mensajerocliente.services.Application;
import com.fiuba.mensajerocliente.services.RestServiceAsync;
import com.fiuba.mensajerocliente.session.SessionManager;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.otto.Subscribe;


public class WriteMsgDialog extends AlertDialog.Builder {
    private Context context;
    private View dialogView;
    private AlertDialog alertDialog;
    private TextView msgTo;
    private MaterialEditText msgContent;
    private SessionManager session;
    private String userTo;
    private boolean broadcast;
    private ProgressBar prgrsBar;
    private MessageAdapter adapter;
    private MessageFragment messageFragment;

    public WriteMsgDialog(FragmentActivity activity, MessageAdapter msgAdapter, String userTo, boolean broadcast) {
        super(activity);

        this.userTo = userTo;
        this.adapter = msgAdapter;
        this.context = activity;
        this.broadcast = broadcast;
        LayoutInflater inflater = activity.getLayoutInflater();
        dialogView = inflater.inflate(R.layout.write_message_layout, null);

        session = new SessionManager(context);


        prgrsBar = (ProgressBar) dialogView.findViewById(R.id.progressBarCircularIndeterminateMessage);

        msgTo = (TextView) dialogView.findViewById(R.id.senderName);
        msgTo.setText(session.getUserName());

        msgContent = (MaterialEditText) dialogView.findViewById(R.id.msgContent);

        setView(dialogView);

        setListener();
        alertDialog = create();
    }

    public WriteMsgDialog(FragmentActivity activity, MessageFragment messageFragment, String userTo, boolean broadcast) {
        super(activity);

        this.userTo = userTo;
        this.messageFragment = messageFragment;
        this.context = activity;
        this.broadcast = broadcast;
        LayoutInflater inflater = activity.getLayoutInflater();
        dialogView = inflater.inflate(R.layout.write_message_layout, null);

        session = new SessionManager(context);


        prgrsBar = (ProgressBar) dialogView.findViewById(R.id.progressBarCircularIndeterminateMessage);

        msgTo = (TextView) dialogView.findViewById(R.id.senderName);
        msgTo.setText(session.getUserName());

        msgContent = (MaterialEditText) dialogView.findViewById(R.id.msgContent);

        setView(dialogView);

        setListener();
        alertDialog = create();
    }

    public void showDialog() {
        alertDialog.show();
    }

    private void setListener() {
        ImageView buttonAccept = (ImageView) dialogView.findViewById(R.id.sendMsg);
        buttonAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //TODO: send message task
                if (!broadcast)
                    postMessage();
                else
                    postBroadcast();
                // alertDialog.dismiss();
                // reset();
            }
        });

        ImageView buttonClear = (ImageView) dialogView.findViewById(R.id.dismissMsg);
        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });
    }


    public void reset() {
        msgContent.setText("");
    }

    @Subscribe
    public void onResponseClient(Message msg) {
        Application.getEventBus().unregister(this);
        reset();
        //adapter.addMsg(msg);
        //this.context.getMessages();
        alertDialog.dismiss();
    }


    @Subscribe
    public void onResponseBroadcast(BroadcastResponse broadcast) {
        Application.getEventBus().unregister(this);
        reset();
        //if (broadcast.succes)
        //{
        Toast.makeText(context, "Mensaje enviado a " +  broadcast.usersSend + " usuarios", Toast.LENGTH_SHORT).show();
        //}
        //adapter.addMsg(msg);
        //this.context.getMessages();
        messageFragment.getMessages();
        alertDialog.dismiss();

    }

    //Se llama a este metodo en caso de que la api devuelva cualquier tipo de error
    @Subscribe
    public void onResponse(ResponseError responseError) {
        Toast.makeText(context, "Hubo un error al crear un mensaje." + responseError.reason, Toast.LENGTH_SHORT).show();

        Application.getEventBus().unregister(this);
    }



    public void postMessage() {
        Application.getEventBus().register(this);


        RestServiceAsync.GetResult result = new RestServiceAsync.GetResult<Message, ApiService>() {
            @Override
            public Message getResult(ApiService service) {
                return service.postMessage(new Message(msgContent.getText().toString(),session.getUserName(),userTo ,Constants.MsgCardType.text));
            }
        };

        RestClient restClient = new RestClient();

        RestServiceAsync callApi = new RestServiceAsync<Message, ApiService>();
        callApi.fetch(restClient.getApiService(), result, new ResponseError());

    }

    public void postBroadcast() {
        Application.getEventBus().register(this);


        RestServiceAsync.GetResult result = new RestServiceAsync.GetResult<BroadcastResponse, ApiService>() {
            @Override
            public BroadcastResponse getResult(ApiService service) {
                return service.broadcast(new Message(msgContent.getText().toString(), session.getUserName(), userTo, Constants.MsgCardType.text));
            }
        };

        RestClient restClient = new RestClient();

        RestServiceAsync callApi = new RestServiceAsync<BroadcastResponse, ApiService>();
        callApi.fetch(restClient.getApiService(), result, new ResponseError());

    }




}
