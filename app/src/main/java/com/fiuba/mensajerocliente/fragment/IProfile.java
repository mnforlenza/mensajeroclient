package com.fiuba.mensajerocliente.fragment;


import com.fiuba.mensajerocliente.dto.User;

public interface IProfile {
    public void setUser(User user);
}
