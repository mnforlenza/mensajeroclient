package com.fiuba.mensajerocliente.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fiuba.mensajerocliente.ContactConversation;
import com.fiuba.mensajerocliente.ContactLocation;
import com.fiuba.mensajerocliente.ProfileReduced;
import com.fiuba.mensajerocliente.R;
import com.fiuba.mensajerocliente.adapter.ContactAdapter;
import com.fiuba.mensajerocliente.dto.Message;
import com.fiuba.mensajerocliente.dto.ResponseError;
import com.fiuba.mensajerocliente.dto.User;
import com.fiuba.mensajerocliente.dto.UserList;
import com.fiuba.mensajerocliente.extras.Constants;
import com.fiuba.mensajerocliente.extras.RecyclerItemClickListener;
import com.fiuba.mensajerocliente.client.RestClient;
import com.fiuba.mensajerocliente.services.ApiService;
import com.fiuba.mensajerocliente.services.Application;
import com.fiuba.mensajerocliente.services.RestServiceAsync;
import com.fiuba.mensajerocliente.session.SessionManager;
import com.google.gson.Gson;
import static com.fiuba.mensajerocliente.extras.Constants.USER;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;



public class ContactFragment extends Fragment {
    private ListView listViewContacts;
    private ContactAdapter contactAdapter;
    private RecyclerView recyclerView;
    private View myView;
    private EditText searchText;
    private ProgressBar prgrsBar;
    private TextView emptyView;
    private SessionManager session;

    public static ContactFragment newInstance(String param1, String param2) {
        ContactFragment fragment = new ContactFragment();
        Bundle args = new Bundle();
        //put any extra arguments that you may want to supply to this fragment
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.contact_fragment, container, false);

        session = new SessionManager(getActivity().getApplicationContext());

        setHasOptionsMenu(true);

        /*
        ImageView buttonSearch = (ImageView) myView.findViewById(R.id.search);
        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //searchUsers(false);
            }
        });

        ImageView buttonClear= (ImageView) myView.findViewById(R.id.search_clear);
        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //searchClear(v);
            }
        });
                //Se agrega esto por que sino no funciona el input con tabs
        searchText = (EditText) myView.findViewById(R.id.search_text);
        searchText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                searchText.requestFocusFromTouch();
                return false;
            }
        });
        */

        prgrsBar = (ProgressBar) myView.findViewById(R.id.progressBarCircularIndeterminate);
        emptyView = (TextView) myView.findViewById(R.id.empty_view);


        recyclerView = (RecyclerView) myView.findViewById(R.id.listViewContacts);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        contactAdapter = new ContactAdapter(getActivity());
        //addAllContacts(fillContacts());
        recyclerView.setAdapter(contactAdapter);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        User user = contactAdapter.getContact(position);
                        Message conversation = new Message("", session.getUserName(), user.userName, Constants.MsgCardType.text);
                        Intent intent = new Intent(getActivity(), ContactConversation.class);
                        intent.putExtra("CONVERSATION", new Gson().toJson(conversation));
                        startActivity(intent);
                    }

                    @Override
                    public void onItemLongClick(View view, int position) {

                        User user = contactAdapter.getContact(position);
                        if (user != null) {
                            optionsContact(user);
                        }
                    }
                })
        );



        contactAdapter.setContacts(new ArrayList<User>(),session.getUserName());
        searchUsers(true);


        return myView;
    }


    private void optionsContact(final User user)
    {

        final List<String> listItems = new ArrayList<String>();
        listItems.add("Perfil");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Opciones del contacto");
            final CharSequence options[] = listItems.toArray(new CharSequence[listItems.size()]);

            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    if (options[which].equals("Perfil")) {
                        loadProfile(user);
                    }
                }
            });
            builder.show();
    }

    public void loadProfile(User contact) {
        Intent intent = new Intent(getActivity(), ProfileReduced.class);

        intent.putExtra(USER, new Gson().toJson(contact));
        startActivity(intent);
    }

    public void searchUsers(boolean friend) {
        getUsers();
    }

    public void searchClear(View view) {
        emptyView.setVisibility(View.INVISIBLE);
        searchText.setText("");
        //searchUsers(true);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_location).setVisible(true);
        return;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_location:
                Intent intent = new Intent(getActivity(), ContactLocation.class);
                startActivity(intent);
                break;
            default:
                break;
        }
        return true;
    }



    @Override
    public void onPause() {
        super.onPause();
        Application.getEventBus().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.getEventBus().register(this);
    }

    @Subscribe
    public void onUserResponse(UserList userList) {
            if(userList.users.isEmpty())
                emptyView.setVisibility(View.VISIBLE);
            else
                emptyView.setVisibility(View.INVISIBLE);

        prgrsBar.setVisibility(View.INVISIBLE);
        contactAdapter.setContacts(userList.users,session.getUserName());
    }

    //Se llama a este metodo en caso de que la api devuelva cualquier tipo de error
    @Subscribe
    public void onResponse(ResponseError responseError) {
        emptyView.setVisibility(View.INVISIBLE);
        prgrsBar.setVisibility(View.INVISIBLE);
        Toast.makeText(getActivity(), "Hubo un error al obtener los datos del usuario." + responseError.reason, Toast.LENGTH_SHORT).show();
    }



    public void getUsers() {

        prgrsBar.setEnabled(true);
        prgrsBar.setVisibility(View.VISIBLE);

        RestServiceAsync.GetResult result = new RestServiceAsync.GetResult<UserList, ApiService>() {
            @Override
            public UserList getResult(ApiService service) {
                return service.getUsers();
            }
        };

        RestClient restClient = new RestClient();

        RestServiceAsync callApi = new RestServiceAsync<UserList, ApiService>();
        callApi.fetch(restClient.getApiService(), result, new ResponseError());

    }


}
