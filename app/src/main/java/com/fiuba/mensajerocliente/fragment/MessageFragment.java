package com.fiuba.mensajerocliente.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fiuba.mensajerocliente.ContactConversation;
import com.fiuba.mensajerocliente.ContactMessage;
import com.fiuba.mensajerocliente.R;
import com.fiuba.mensajerocliente.adapter.ConversationAdapter;
import com.fiuba.mensajerocliente.client.RestClient;
import com.fiuba.mensajerocliente.dto.Message;
import com.fiuba.mensajerocliente.dto.MessageList;
import com.fiuba.mensajerocliente.dto.ResponseError;
import com.fiuba.mensajerocliente.extras.RecyclerItemClickListener;
import com.fiuba.mensajerocliente.services.ApiService;
import com.fiuba.mensajerocliente.services.Application;
import com.fiuba.mensajerocliente.services.RestServiceAsync;
import com.fiuba.mensajerocliente.session.SessionManager;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;


public class MessageFragment extends Fragment
{
    private View myView;
    private ConversationAdapter conversationAdapter;
    private RecyclerView recyclerView;
    private ProgressBar prgrsBar;
    private TextView emptyView;
    private SessionManager session;
    private EditText searchText;
    private WriteMsgDialog w_broadcastDialog;

    public static MessageFragment newInstance(String param1, String param2) {
        MessageFragment fragment = new MessageFragment();
        Bundle args = new Bundle();
        //put any extra arguments that you may want to supply to this fragment
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        myView = inflater.inflate(R.layout.message_fragment, container, false);

        session = new SessionManager(getActivity().getApplicationContext());

        prgrsBar = (ProgressBar) myView.findViewById(R.id.progressBarCircular_Msgs);
        emptyView = (TextView) myView.findViewById(R.id.empty_view_msg);

     /*   ImageView buttonSearch = (ImageView) myView.findViewById(R.id.search_conversation_button);
        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: search update
            }
        });

        ImageView buttonClear= (ImageView) myView.findViewById(R.id.search_conversation_clear);
        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchText.setText("");
                //TODO: search update
            }
        });

        //Se agrega esto por que sino no funciona el input con tabs
        searchText = (EditText) myView.findViewById(R.id.search_conversation);
        searchText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                searchText.requestFocusFromTouch();
                return false;
            }
        });
         */
        final FloatingActionButton button_actionAddMsg = (FloatingActionButton) myView.findViewById(R.id.action_writeMsg);
        button_actionAddMsg.setVisibility(View.GONE);

        final FloatingActionButton button_actionAddPlace = (FloatingActionButton) myView.findViewById(R.id.action_addPlace);
        button_actionAddPlace.setVisibility(View.GONE);

        final FloatingActionButton button_actionAddConversation = (FloatingActionButton) myView.findViewById(R.id.action_startConversation);
        final FloatingActionButton button_actionAddBroadcast = (FloatingActionButton) myView.findViewById(R.id.action_broadcast);

        button_actionAddConversation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ContactMessage.class);
                startActivity(intent);
            }
        });

        w_broadcastDialog = new WriteMsgDialog(this.getActivity(), this,"",true);

        button_actionAddBroadcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                w_broadcastDialog.showDialog();
            }
        });

        recyclerView = (RecyclerView) myView.findViewById(R.id.listViewConversations);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        conversationAdapter = new ConversationAdapter(getActivity());

        recyclerView.setAdapter(conversationAdapter);

        conversationAdapter.setConversations(new ArrayList<Message>(), session.getUserid());

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Message conversation = conversationAdapter.getConversation(position);
                        Intent intent = new Intent(getActivity(), ContactConversation.class);
                        intent.putExtra("CONVERSATION", new Gson().toJson(conversation));
                        startActivity(intent);
                    }
                })
        );



        return myView;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        getMessages();

    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Subscribe
    public void onMessagesResponse(MessageList msgs) {
        Application.getEventBus().unregister(this);

        if (!msgs.messages.isEmpty())
            conversationAdapter.setConversations(msgs.messages,"");



    }

    //Se llama a este metodo en caso de que la api devuelva cualquier tipo de error
    @Subscribe
    public void onResponse(ResponseError responseError) {
        Application.getEventBus().unregister(this);

        Toast.makeText(getActivity(), "Hubo un error al crear un mensaje." + responseError.reason, Toast.LENGTH_SHORT).show();
    }



    public void getMessages() {
        Application.getEventBus().register(this);

        RestServiceAsync.GetResult result = new RestServiceAsync.GetResult<MessageList, ApiService>() {
            @Override
            public MessageList getResult(ApiService service) {
                return service.getLastMessages(session.getUserName());
            }
        };

        RestClient restClient = new RestClient();

        RestServiceAsync callApi = new RestServiceAsync<MessageList, ApiService>();
        callApi.fetch(restClient.getApiService(), result, new ResponseError());

    }


}
