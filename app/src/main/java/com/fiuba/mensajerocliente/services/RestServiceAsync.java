package com.fiuba.mensajerocliente.services;

import android.os.AsyncTask;

import com.fiuba.mensajerocliente.dto.ResponseError;

import retrofit.RetrofitError;


public class RestServiceAsync<ReturnType, RestService> {

    public static abstract class GetResult<ReturnResult, ServiceClass>  {
        public abstract ReturnResult getResult(ServiceClass mService);
    }

    public void fetch(
            final RestService service,
            final GetResult getResult,
            final ResponseError errorResponse
    ) {
        new AsyncTask<Void, Void, ReturnType>() {

            @Override
            protected ReturnType doInBackground(Void... params) {
                try {

                    ReturnType res = (ReturnType) getResult.getResult(service);
                    if(res!=null) {
                    }
                    return res;
                } catch (RetrofitError retroError) {
                    if (retroError.getResponse()!=null)
                    {
                        errorResponse.status = retroError.getResponse().getStatus();
                        errorResponse.reason = retroError.getResponse().getReason();
                    }
                    errorResponse.status = 400;
                    errorResponse.reason = "Problema de conexion";

                    return null;
                } catch(Exception e) {
                    return null;
                }
            }
            @Override
            protected void onPostExecute(ReturnType res) {
                if(res!=null) {
                    Application.getEventBus().post(res);
                } else /*if(errorResponse!=null)*/ {
                    Application.getEventBus().post(errorResponse);
                }
            }
        }.execute();
    }

}