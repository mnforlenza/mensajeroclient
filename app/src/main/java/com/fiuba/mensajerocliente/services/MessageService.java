package com.fiuba.mensajerocliente.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.fiuba.mensajerocliente.client.RestClient;
import com.fiuba.mensajerocliente.dto.Message;
import com.fiuba.mensajerocliente.dto.ResponseError;
import com.fiuba.mensajerocliente.session.SessionManager;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MessageService extends Service {

    private IBinder mBinder = new SocketServerBinder();
    private Timer mTimer;
    private boolean mRunning = false;
    private String userID;
    private SessionManager sessionManager;

    @Override
    public void onCreate() {
        super.onCreate();
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {

            @Override
            public void run() {

                if (mRunning) {
                    getMessages();
                }
            }
        }, 1000, 1000);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mRunning = true;
        userID = intent.getStringExtra("userTo");
        sessionManager = new SessionManager(getApplicationContext());

        getMessages();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent arg0) {
        mRunning = true;
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        mRunning = false;
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        mRunning = false;
        super.onDestroy();
    }

    public void getMessages() {
        RestServiceAsync.GetResult result = new RestServiceAsync.GetResult<List<Message>, ApiService>() {
            @Override
            public List<Message> getResult(ApiService service) {
                return service.getMessages(sessionManager.getUserName(),userID);
            }
        };

        RestClient restClient = new RestClient();

        RestServiceAsync callApi = new RestServiceAsync<List<Message>, ApiService>();
        callApi.fetch(restClient.getApiService(), result, new ResponseError());

    }
    public class SocketServerBinder extends Binder {

        public MessageService getService() {
            return MessageService.this;
        }

    }
}
