package com.fiuba.mensajerocliente.services;

import com.fiuba.mensajerocliente.dto.BroadcastResponse;
import com.fiuba.mensajerocliente.dto.Checkin;
import com.fiuba.mensajerocliente.dto.Message;
import com.fiuba.mensajerocliente.dto.MessageList;
import com.fiuba.mensajerocliente.dto.Response;
import com.fiuba.mensajerocliente.dto.User;
import com.fiuba.mensajerocliente.dto.UserResponse;
import com.fiuba.mensajerocliente.dto.UserList;

import java.util.List;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Query;

public interface ApiService {

    @POST("/api/user/create")
    public Response create(
               @Body User user
    );

    @POST("/api/user/login")
    public Response login(
            @Body User user
    );


    @POST("/api/user/logout")
    public Response logout(
            @Body User user
    );


    @GET("/api/user")
    public UserResponse get(
            @Query("userName") String userName
    );


    @PUT("/api/user")
    public UserResponse updateUser(
            @Body User user
    );


    @GET("/api/user/all")
    public UserList getUsers();


    @POST("/api/msg/create")
    public Message postMessage(
            @Body Message message
    );

    @POST("/api/msg/broadcast")
    public BroadcastResponse broadcast(
            @Body Message message
    );

    @GET("/api/msg/all")
    public List<Message> getMessages(
            @Query("userFrom") String userFrom,
            @Query("userTo") String userTo
    );

    @GET("/api/msg/last")
    public MessageList getLastMessages(
            @Query("userName") String userName
    );

    @GET("/api/user/online")
    public Response online(
            @Query("userName") String userName
    );

    @PUT("/api/user/checkin")
    public UserResponse userCheckin(
            @Body Checkin checkin
    );


}
