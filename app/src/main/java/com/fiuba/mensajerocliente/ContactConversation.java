package com.fiuba.mensajerocliente;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dexafree.materialList.view.MaterialListView;
import com.fiuba.mensajerocliente.adapter.MessageAdapter;
import com.fiuba.mensajerocliente.client.RestClient;
import com.fiuba.mensajerocliente.dto.Message;
import com.fiuba.mensajerocliente.dto.ResponseError;
import com.fiuba.mensajerocliente.fragment.WriteMsgDialog;
import com.fiuba.mensajerocliente.services.ApiService;
import com.fiuba.mensajerocliente.services.Application;
import com.fiuba.mensajerocliente.services.MessageService;
import com.fiuba.mensajerocliente.services.RestServiceAsync;
import com.fiuba.mensajerocliente.session.SessionManager;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;


public class ContactConversation extends ActionBarActivity
{
    private MessageAdapter msgsAdapter;
    private MaterialListView messagesList;
    private ProgressBar prgrsBar;
    private TextView emptyView;
    private SessionManager session;
    private WriteMsgDialog writeMsgDialog;
    private Toolbar toolbar;
    private Message conversation;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.conversation_layout);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });


        session = new SessionManager(getApplicationContext());

        prgrsBar = (ProgressBar) findViewById(R.id.progressBarCircularIndeterminateConversation);
        emptyView = (TextView) findViewById(R.id.empty_view);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setReverseLayout(false);
        mLayoutManager.setStackFromEnd(true);

        messagesList = (MaterialListView) findViewById(R.id.listViewMsgs);
        messagesList.setLayoutManager(mLayoutManager);

        msgsAdapter = new MessageAdapter(this.getApplicationContext(), messagesList, session.getUserid());

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            String forumJson = extras.getString("CONVERSATION");
            conversation = new Gson().fromJson(forumJson, Message.class);
            getSupportActionBar().setTitle(conversation.userTo);
        }

        writeMsgDialog = new WriteMsgDialog(this, this.msgsAdapter,conversation.userTo,false);
        final FloatingActionButton button_actionWriteMeg = (FloatingActionButton) findViewById(R.id.action_writeMsg);
        button_actionWriteMeg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeMsgDialog.showDialog();
            }
        });

        final FloatingActionButton button_actionAddConversation = (FloatingActionButton) findViewById(R.id.action_startConversation);
        button_actionAddConversation.setVisibility(View.GONE);

        final FloatingActionButton button_actionAddBroadcast = (FloatingActionButton) findViewById(R.id.action_broadcast);
        button_actionAddBroadcast.setVisibility(View.GONE);

        FloatingActionButton button_actionAddPlace = (FloatingActionButton) findViewById(R.id.action_addPlace);
        button_actionAddPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MapMessage.class);
                intent.putExtra("CONVERSATION", new Gson().toJson(conversation));
                startActivity(intent);
            }
        });
        Application.getEventBus().register(this);

        Intent intent = new Intent(this, MessageService.class);
        intent.putExtra("userTo", conversation.userTo);
        startService(intent);
    }

    @Override
    public void onPause() {
        super.onPause();
        Intent intent = new Intent(this, MessageService.class);
        stopService(intent);
        Application.getEventBus().unregister(this);


    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Subscribe
    public void onMessagesResponse(ArrayList<Message> msgs) {
        if (!msgs.isEmpty())
        {
            msgsAdapter.setData(msgs);
            msgsAdapter.fillArray();

        }
        //Application.getEventBus().unregister(this);


    }

    //Se llama a este metodo en caso de que la api devuelva cualquier tipo de error
    @Subscribe
    public void onResponse(ResponseError responseError) {
        Toast.makeText(this, "Hubo un error al crear un mensjae." + responseError.reason, Toast.LENGTH_SHORT).show();
        //Application.getEventBus().unregister(this);

    }



    public void getMessages() {
        Application.getEventBus().register(this);

        RestServiceAsync.GetResult result = new RestServiceAsync.GetResult<List<Message>, ApiService>() {
            @Override
            public List<Message> getResult(ApiService service) {
                return service.getMessages(session.getUserName(),conversation.userTo);
            }
        };

        RestClient restClient = new RestClient();

        RestServiceAsync callApi = new RestServiceAsync<List<Message>, ApiService>();
        callApi.fetch(restClient.getApiService(), result, new ResponseError());

    }

}