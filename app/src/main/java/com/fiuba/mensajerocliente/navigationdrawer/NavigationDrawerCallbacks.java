package com.fiuba.mensajerocliente.navigationdrawer;


public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
