package com.fiuba.mensajerocliente;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fiuba.mensajerocliente.client.RestClient;
import com.fiuba.mensajerocliente.dto.Response;
import com.fiuba.mensajerocliente.dto.ResponseError;
import com.fiuba.mensajerocliente.dto.User;
import com.fiuba.mensajerocliente.dto.UserResponse;
import com.fiuba.mensajerocliente.fragment.BoardFragment;
import com.fiuba.mensajerocliente.navigationdrawer.NavigationDrawerCallbacks;
import com.fiuba.mensajerocliente.navigationdrawer.NavigationDrawerFragment;
import com.fiuba.mensajerocliente.services.ApiService;
import com.fiuba.mensajerocliente.services.Application;
import com.fiuba.mensajerocliente.services.RestServiceAsync;
import com.fiuba.mensajerocliente.session.SessionManager;
import com.squareup.otto.Subscribe;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.fiuba.mensajerocliente.extras.Utils.getPhotoString;

/**
 * Created by gonzalovelasco on 29/3/15.
 */
public class Main extends ActionBarActivity  implements NavigationDrawerCallbacks {
    private SweetAlertDialog pDialog;
    private Toolbar toolbar;
    private NavigationDrawerFragment drawerFragment;
    SessionManager session;
    private boolean init;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        session = new SessionManager(getApplicationContext());

        TextView userMail = (TextView)findViewById(R.id.txtUserEmail);
        TextView userName = (TextView)findViewById(R.id.txtCompleteName);


        userMail.setText(session.getUserMail());
        userName.setText(session.getUserName()+" "+session.getUserSurname());
        String photo = session.getUserPhoto();
        if (!photo.isEmpty())
        {
            ImageView imageAvatar = (ImageView)findViewById(R.id.imgAvatar);
            byte[] decodedString = Base64.decode(photo, Base64.DEFAULT);
            imageAvatar.setImageBitmap(BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length));
        }



        drawerFragment = (NavigationDrawerFragment)getFragmentManager().findFragmentById(R.id.fragment_drawer);
        drawerFragment.setup(R.id.fragment_drawer,(DrawerLayout)findViewById(R.id.drawer_layout), toolbar);


    }

    /*
       -Aqui se crean los fragments que se lanzan del menu desplegable
   */
    @Override
    public void onNavigationDrawerItemSelected(int position) {
        Fragment fragment = null;
        FragmentManager fragmentManager = getSupportFragmentManager();
        Intent intent = null;

        if(!init) {
            init = true;
            fragmentManager.beginTransaction().replace(R.id.container,new BoardFragment()).commit();
            return;
        }

        switch (position) {
            case 0: /* PERFIL */
                intent = new Intent(Main.this,Profile.class);
                startActivity(intent);
                break;
            case 1: /* CONFIGURACION */
               // intent = new Intent(Main.this,Configuration.class);
               // startActivity(intent);
                break;
            case 2:  /* SALIR */
                shutdown();
                break;
            default: break;
        }
    }

    private void shutdown() {
        logout();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem itemSubmit = menu.findItem(R.id.action_submit);
        itemSubmit.setVisible(false);
        MenuItem itemSearch = menu.findItem(R.id.action_edit);
        itemSearch.setVisible(false);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        if (drawerFragment.isDrawerOpen())
            drawerFragment.closeDrawer();
        else
            super.onBackPressed();
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Subscribe
    public void onResponse(Response response) {
        Application.getEventBus().unregister(this);
        session.logoutUser();

    }

    //Se llama a este metodo en caso de que la api devuelva cualquier tipo de error
    @Subscribe
    public void onResponse(ResponseError responseError) {
        Application.getEventBus().unregister(this);
        session.logoutUser();
        Toast.makeText(this, "Hubo un error al desloguearse." + responseError.reason, Toast.LENGTH_SHORT).show();
        //Application.getEventBus().unregister(this);

    }



    public void logout() {

        Application.getEventBus().register(this);

        RestServiceAsync.GetResult result = new RestServiceAsync.GetResult<Response, ApiService>() {
            @Override
            public Response getResult(ApiService service) {
                return service.logout(new User(session.getToken(), ""));
            }
        };

        RestClient restClient = new RestClient();

        RestServiceAsync callApi = new RestServiceAsync<Response, ApiService>();
        callApi.fetch(restClient.getApiService(), result, new ResponseError());

    }

}
