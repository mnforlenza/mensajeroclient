package org.fiuba.taller2.mensajeroclient.service;

import org.fiuba.taller2.mensajeroclient.util.HttpUtils;
import org.fiuba.taller2.mensajeroclient.util.JsonUtils;

public class UserServiceImpl implements UserService {

	private static final String PATH = "user/";

	private static final String IS_USER_ONLINE_PATH = "online/";

	@Override
	public boolean isUserOnline(String userName) {
		return ((Boolean) JsonUtils.jsonToObject(HttpUtils.get(PATH
				+ IS_USER_ONLINE_PATH + "{" + userName + "}")));
	}

}
