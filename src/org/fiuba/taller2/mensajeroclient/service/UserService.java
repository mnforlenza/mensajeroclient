package org.fiuba.taller2.mensajeroclient.service;

public interface UserService {
	
	boolean isUserOnline(String userName);
}
