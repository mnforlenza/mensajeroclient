package org.fiuba.taller2.mensajeroclient.model;

public class User {
	
	private String userName;
	
	private String password;
	
	private String email;
	
	private boolean active;
	
	private boolean online;

	public User(String userName, String password) {
		super();
		this.userName = userName;
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}

	public String getEmail() {
		return email;
	}

	public boolean isActive() {
		return active;
	}

	public boolean isOnline() {
		return online;
	}
	
	public void setEmail(String email){
		this.email = email;
	}
	
	public void activeUser(){
		this.active = true;
	}
	
	public void setOnline(){
		this.online = true;
	}
	
	public void setOffline(){
		this.online = false;
	}

}
